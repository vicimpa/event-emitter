interface IListener {
  (...data: any[]): void
  once: boolean
}

class EventEmitter{
  // Объект событий
  private _events: any = {}
  // Зарегистрированные на сторону события
  private _registred: any = {}

  // Конструктор
  constructor(private _register: EventTarget = null){}

  public on(type: string, listener: IListener): IListener{
    let listeners: IListener[] = this._events[type]

    if(!Array.isArray(listeners))
      this._events[type] = listeners = []

    let index = listeners.indexOf(listener)

    if(index === -1)
      listeners.push(listener)

    if(this._register && !this._registred[type]){
      this._registred[type] = (...data: any[]) => this.emit(type, ...data, false)
      this._register.addEventListener(type, this._registred[type])
    }

    return listener
  }

  public off(type: string, listener: IListener): IListener{
      let listeners: IListener[] = this._events[type]

      if(!Array.isArray(listeners))
        this._events[type] = listeners = []

      if(listener){
        let index = listeners.indexOf(listener)

        if(index !== -1)
          listeners.splice(index, 1)
      }else{
        this._events[type] = listeners = []
      }

      if(this._register && this._registred[type] && !listeners.length){
        this._register.removeEventListener(type, this._registred[type])
        delete this._registred[type]
      }

      return listener
  }

  public emit(type: string, ...data: any[]){
      let listeners: IListener[] = this._events[type]

      if(!Array.isArray(listeners))
        this._events[type] = listeners = []

      if(!this._register || data[data.length - 1] === false){
        for(let listener of listeners){
          listener.apply(this, data)

          if(listener.once)
            this.off(type, listener)
        }
      }

      if(this._register && data[data.length - 1] !== false){
          if(typeof this._register[type] === 'function')
            return this._register[type]()

          this._register.dispatchEvent(new Event(type))
      }
  }

  public once(type: string, listener: IListener): IListener{
    listener.once = true
    return this.on(type, listener)
  }

  public registerTarget(target: EventTarget){
    if(this._register)
      for(let type in this._registred)
        this._register.removeEventListener(type, this._registred[type])

    this._register = target

    for(let type in this._events){
        this._registred[type] = (...data: any[]) => this.emit(type, ...data, false)
        this._register.addEventListener(type, this._registred[type])
    }
  }
}
