# EventEmitter для браузеров

---------------
###*Использование:*
```js
// Постоянный обработчик
emitter.on('click', listener)  

// Временный обработчик
// При этом если будет передан уже существующий постоянный обработчик, он будет перезаписан во временный
emitter.once('click', listener)

// Снятия обработчика
emitter.off('click', listener)

// Снятия всех обработчиков
emitter.off('click')
```

---------------

###*Создание без элемента:*
```js
// Содадим экземпляр EventEmitter
var emitter = new EventEmitter()           
```

---------------

###*Явное создание с элементом:*
```js
// Получим элемент
var element = document.querySelector('button')    

// Содадим экземпляр EventEmitter
var emitter = new EventEmitter(element)           
```
 
---------------

###*Не явное создание с элементом:*
```js
// Содадим экземпляр EventEmitter
var emitter = new EventEmitter()      

// Получим элемент
var element = document.querySelector('button')    

// Зарегистрируем элемент               
// При этом если у EventEmitter в этот момент были подписаны события без элемента
// они будт переподписаны на эелемен
// Если события были подписаны с другим элементом, то они будут отписаны от старого и подписаны на новый
emitter.registerTarget(element)       
```
